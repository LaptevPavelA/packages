
//////Перевод скорости из км/ч в м/с (чистый js)
	var speedFormat = () => {
		
		let speed = document.getElementById('speed').value.replace(",",".");
		if (+speed > 0){
			let float = document.getElementById('float').value ? document.getElementById('float').value : 2;
			let speedFormat = (speed/3.6).toFixed(float);
			document.querySelector('#speedFormat').innerHTML = speedFormat + " м/с.";
			document.querySelector('#error').innerHTML = "";
		} else {
			document.querySelector('#error').innerHTML = "значение должно быть корректным числом больше 0";
			document.getElementById('error').style.color = "red";
			document.querySelector('#speedFormat').innerHTML = '';
		}
	}


////////Определение знака зодиака (чистый js)
	function zodiac () {
			this.signs = [
					        ['Козерог', '01-01', '01-20'], // 01.01-20.01
					        ['Водолей', '01-21', '02-20'], // 21.01-20.02
					        ['Рыбы', '02-21', '03-20'], // 21.02-20.03
					        ['Овен', '03-21', '04-20'], // 21.03-20.04
					        ['Телец', '04-21', '05-20'], // 21.04-20.05
					        ['Близнецы', '05-21', '06-21'], // 21.05-21.06
					        ['Рак', '06-22', '07-22'], // 22.06-22.07
					        ['Лев', '07-23', '08-23'], // 23.07-23.08
					        ['Дева', '08-24', '09-23'], // 24.08-23.09
					        ['Весы', '09-24', '10-23'], // 24.09-23.10
					        ['Скорпион', '10-24', '11-22'], // 24.10-22.11
					        ['Стрелец', '11-23', '12-21'], // 23.11-21.12
					        ['Козерог', '12-22','12-31' ] // 22.12-01.01
						    ];


			this.learnSign = (date) => {
	 			let year = date.substr(0,4);
	 			date = new Date(date);

	 			for (var i = 0; i < this.signs.length; i++) {
	 				this.signs[i][1] = new Date(year + '-' + this.signs[i][1]);
	 				this.signs[i][2] = new Date(year + '-' + this.signs[i][2]);
	 			}
	 			let zodiac = this.signs.filter(sign => {
	 				if (+date >= +sign[1] && +date <= +sign[2])
	 					return true;
	 			})

	 			return zodiac[0][0];

	  		};
		}


	var learnZodiac = () => {
		if (document.getElementById('dateInput').value === "" ) {
			document.querySelector('#errorDate').innerHTML = "для определения знака необходимо ввести дату";
			document.getElementById('errorDate').style.color = "red";
		
		}else if ((new Date(document.getElementById('dateInput').value) > new Date (9999,12,31))){
			document.querySelector('#errorDate').innerHTML = "максимальная дата 31.12.9999";
			document.getElementById('errorDate').style.color = "red";
		}else {
			document.querySelector('#errorDate').innerHTML = "";		
			let obj = new zodiac();
			let date = document.getElementById('dateInput').value; 
			document.querySelector('#zodiac').innerHTML = obj.learnSign(date);
		}
		//console.log(+new Date (document.getElementById('dateInput').value));
	}
///////////Форма авторизации (JQuery)
	var logIn = () => {
		let login = $('#log').html();
		let password = $('#pass').html();
		if (($('#login').val() === login && $('#password').val() === password)){
			$(location).attr('href','./login.html')
		} else {
			$('#errorForm').html('<h style="color: red;">Неверная пара login-password</h>');
			$('#login').val('');
			$('#password').val('');

		}
				
				
	}

//////временная смена пароля без бэкенда
	var newPasswordForm = () => {
		$('#loginForm').hide();
		$('#newPasswordForm').show();
		$('#errorForm').html('');
	}

	var cancelPasswordForm = () => {
		$('#loginForm').show();
		$('#newPasswordForm').hide();
		$('#errorFormNewPassword').html('');
	}

	var newPassword = () => {		
		let login = $('#log').html();
		let password = $('#pass').html();
		if ($('#loginOld').val() !== login || $('#passwordOld').val() !== password){
			$('#errorFormNewPassword').html('<h style="color: red;">Неверная пара login-password</h>');
			$('#loginOld').val('');
			$('#passwordOld').val('');

		}else if (($('#passwordNew').val()).length < 3) {
			$('#errorFormNewPassword').html('<h style="color: red;">Пароль не может быть меньше 4 символов</h>');
			$('#passwordNew').val('');
			$('#passwordConfirm').val('');
			
		}else if ($('#passwordNew').val() !== $('#passwordConfirm').val()){
			$('#errorFormNewPassword').html('<h style="color: red;">Пароли не совпадают</h>');
			$('#passwordNew').val('');
			$('#passwordConfirm').val('');

		} else{
			$('#pass').html($('#passwordNew').val());
			$('#passNewForm').html($('#passwordNew').val());
			cancelPasswordForm();

		}
	}
	
	
